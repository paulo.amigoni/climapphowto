# ClimAppHowTo
# Integração com Sistemas Remotos
## Consumindo REST API

Neste exemplo iremos realizar uma integração com um sistema remoto e iremos consumir os dados de uma API de previsão de tempo com base no provedor [HGBrasil](https://console.hgbrasil.com/documentation/weather).
Para construir este app, iremos utilizar a api `Volley` para gerenciamento de requisições web, mais informações [aqui](https://developer.android.com/training/volley), `Fused Location Provider API` que é integrada ao the `Google Play Services`, esta api fornece uma forma simplificada para acesso à localização do dispositivo e gerencia o consumo de bateria provendo economia de recursos além de minimizar o código utilizado mais informações [aqui](https://developer.android.com/training/location/retrieve-current); também utilizaremos o framework de permissões necessárias a aplicativos com versões superiores à versão 6 do Android, mais informações [aqui](https://developer.android.com/training/permissions/requesting).

### Criar projeto
Nosso primeiro passo será a criação de um projeto com a limguagem `Kotlin` como base e uma **Activity** do tipo **Empty**, logo após a criação da activity adicione o id para o TextView.

### Adicionar permissão para acesso à INTERNET
No arquivo `app/manifest/Manifest.xml` adicione o código abaixo, apenas a linha abaixo do comentárioÂ

```javascript
<?xml version="1.0" encoding="utf-8"?>
<manifest xmlns:android="http://schemas.android.com/apk/res/android"
    package="br.com.fatecararas.climapphowto">

    <!-- Neste trecho, insira a permissão anstes da TAG <application> -->
    <uses-permission android:name="android.permission.INTERNET"/>

```
### Adicionar a dependência da API Volley
No arquivo `build.graddle(Module App)` adicione ao bloco **dependecy** a dependência do Volley, insira apenas a linha `implementation 'com.android.volley:volley:1.1.1'`.
```javascript
dependencies {
    implementation fileTree(dir: 'libs', include: ['*.jar'])
    implementation "org.jetbrains.kotlin:kotlin-stdlib-jdk7:$kotlin_version"
    implementation 'androidx.appcompat:appcompat:1.1.0'
    implementation 'androidx.core:core-ktx:1.1.0'
    implementation 'androidx.constraintlayout:constraintlayout:1.1.3'
    implementation 'com.android.volley:volley:1.1.1'
    testImplementation 'junit:junit:4.12'
    androidTestImplementation 'androidx.test.ext:junit:1.1.1'
    androidTestImplementation 'androidx.test.espresso:espresso-core:3.2.0'
}
```
### Criar Requisição Web ao provedor HGBrasil
No arquivo `MainActivity.kt`, no método `onCreate` vamos estruturar a requisição.
1. Crie a url de requisição com as coordenadas do `Lago Municipal [-22.3599652,-47.3853482]` como exemplo; adicione o código abaixo.
```javascript
val url = "https://api.hgbrasil.com/weather?key=4e56cf83&lat=-22.3599652&log=-47.3853482"
```
2. Crie a o objeto do tipo `RequestQueue` para adicioná-lo à nossa fila de requisições gerenciadas pelo Volley, ainda no `onCreate`.
```javascript
var queue = Volley.newRequestQueue(this)
```
3. Crie um objeto do tipo StringRequest para realizar a transação de nossa requisição web; este objeto possui dois callbacks principais, um Listener para sucesso e um para falha. Ao criar o objeto você deve passar os seguintes parâmetros: `verbo http`, `url para requisição`, `Response Listener`, o Listener implementa os callbacks para a manipular o resultado da requisição, é devolvido ao Listener um objeto com o resultado, por padrão ele é identificado por `it`, mas você pode renomeá-lo como fizemos em sala de aula; agora é só implementar as ações no corpo dos Listeners com o resultado `it` e logo após adicionar o `stringRequest` à **queue**.
Adicione o código abaixo ao `onCreate`. Lembra do código com `findViewById` para associar o WidGet ao Java, no Kotlin não é necessário.
```javascript
var stringRequest = StringRequest(Request.Method.GET, url,
  Response.Listener<String> {
    textView.text = "Resposta: ${it.substring(0, 500)}"
    Log.d("RESPONSE"," - "+it)
}, Response.ErrorListener {
    textView.text = it.localizedMessage
    Log.d("RESPONSE ERROR"," - "+it.localizedMessage)
})
queue.add(stringRequest)
}
```
4. Adicione as propriedade acima do `onCreate`.
```java
val PERMISSION_ID = 42
var latitude: Double = 0.0
var longitude: Double = 0.0
var listaPrevisoes = ArrayList<Previsao>()
lateinit var dialog: ProgressDialog
lateinit var url: String
lateinit var queue: RequestQueue
lateinit var mFusedLocationClient: FusedLocationProviderClient
```

#### Teste o App para validar o resultado da requisição

### Utilizar a Localização do Dispositivo
Até o passo anterior, realizamos uma requisição web utilizando coordenadas geográficas fixas, porém agora iremos utilizar as coordenadas geográficas obtidas pelo GPS do smartphone utilizando o `Fused Location Client` do Android. 
#### Adicionar permissões ao Manifest
Adicione as permissões para uso de Localização ao `Manifest` conforme trecho de código abaixo.
```javascript
<uses-permission android:name="android.permission.INTERNET" />
<uses-permission android:name="android.permission.ACCESS_COARSE_LOCATION" />
<uses-permission android:name="android.permission.ACCESS_FINE_LOCATION" />
```
#### Fluxo de permissões pós API 26
Criar o fluxo de permissão para uso de localização. Pense o seguinte; as [permissões perigosas](https://developer.android.com/guide/topics/security/permissions.html?hl=pt-br#normal-dangerous) do Android só podem ser utilizada com a permissão explicita do usuário, então precisamos solicitar a permissão e esperar a resposta do usuário para validar se iremos executar nossa função ou não.
Para tal, vamos criar uma função que implemente o fluxo que verifica se existe a necessidade solicitação permissão, solicitando as permissões em sua primeira execução no app; em caso da permissão já estivesse negada, solicitá-la novamente justificando sua necessidade de forma mais explicativa ao usuário.

1. Criar a função que gerencia as solicitações de ações que necessitam de permissão para localização. Adicione o código abaixo.
```java
private fun checkPermissions(vararg permission: String): Boolean {

    val mensagemPermissao = "A localização é necessária para que possamos solicitar " +
            "a previsão de clima em sua localidade."

    /*
    permission é um vararg, ou seja ele pode ser um argumeno, como podem ser varios argumentos,
        em nosso caso ele irá receber permissões para validá-las uma a uma retornando um Boolean
     */
    val havePermission = permission.toList().all {
        ContextCompat.checkSelfPermission(this, it) == PackageManager.PERMISSION_GRANTED
    }

    /*
    Verifica se ha permissoes a solicitar, caso positivo será solicitado ao usuário a permissão
    Se a permissão já foi negada, a solicitação será do tipo RequestPermissionRationale, ou seja
    uma explição mais sugestiva e explicativa deve ser solicitada ao usuário justificando o uso
    de sua localização.
     */
     if (!havePermission) {
             /*
             Este trecho é executado quando a permissão já foi negada, é aqui que devemos
             convencer o usuario da necessidade da permissão para localização do dispositivo.
             */
             if (permission.toList().any {
                     ActivityCompat.shouldShowRequestPermissionRationale(this, it) }) {

                 // Alerta justificando o uso da localização.
                 val alertDialog = AlertDialog.Builder(this)
                     .setTitle("Permission")
                     .setMessage(mensagemPermissao)
                     .setPositiveButton("Ok") { id, v ->
                         run {
                             ActivityCompat.requestPermissions(this, permission, PERMISSION_ID)
                         }
                     }
                     .setNegativeButton("No") { id, v -> }
                     .create()
                 alertDialog.show()
             } else {
               //Na primeira execução do app, esta solicitação é executada
                 ActivityCompat.requestPermissions(this, permission, PERMISSION_ID)
             }
             return false
         }
         return true
}
```
2. Fazer o **override** de `onRequestPermissionsResult`, adicione o trecho de código abaixo na `MainActivity`.
```java
/*
    Após o usuário autorizar ou negar a permissão o método onRequestPermissionsResult é executado e
    todas as permissões passam por aqui, entao devevos selecionar qual é a permissão para poder
    executar as ações necessárias.
     */
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>, grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            PERMISSION_ID -> {
                Log.d("PERMISSION", " - Concedida")
                getLastLocation()
            }
            else -> Log.d("PERMISSION: ", " - Negada")
        }
    }
```
3. Adicione a dependência do Serviço de Localização do Google Play Services ao projeto ao **gradle(Module:app)**. Adicione o trecho abaixo ao conjunto já existente.
```xml
implementation 'com.google.android.gms:play-services-location:17.0.0'
```
Para aqueles que estiverem utilizando a versão mais antiga do Android Studio e tiverem problemas no `Manifest`, utilizem o código abaixo ao invés da versãp **17** acima.
```xml
implementation 'com.google.android.gms:play-services-location:12.0.0'
```
4. Criar função que solicita a localização do usuário. Esta função irá inicializar nosso provedor de localização e obter a localização do dispositivo e após obtê-la realizar a requisição de clima. Adicione o trecho de código abaixo na `MainActivity`.
```java
@SuppressLint("MissingPermission")
    private fun getLastLocation() {
        /*
        Adição do listener/callback de sucesso ao obter a última localização do dispositivo
         */
        mFusedLocationClient?.lastLocation?.addOnSuccessListener { location ->
            if (location == null) {
                Log.e("LOCATION: ", "Erro ao obter Localizacao: ")
            } else {
                /*Aqui recebemos com sucesso a localização utilizamo o método apply(executa bloco de
                código ao receber retorno do listener
                */
                location.apply {
                    // Escreve a localização no LogCat, no tipo Debug[Ainda não vimos o Debug, por enquanto]
                    Log.d("LOCATION: ", location.toString())
                    //Adiciona as coordenadas à url de requisição
                    lat = location.latitude
                    lon = location.longitude

                    Log.d("LOCATION - LATITUDE: ",lat.toString())
                    Log.d("LOCATION - LONGITUDE: ",lon.toString())

                    url =
                        "https://api.hgbrasil.com/weather?key=4e56cf83&lat=${lat}&log=${lon}&user_ip=remote"

                    Log.d("GETLASTLOCATION: ", url)

                    //Chamada à requisição de Clima
                    requestWheater(url)
                }
            }
        }
    }
```
5. Comente o código da requisição com Volley e adicione a verificação de permissão de uso de localização e logo após o método que obtém a localização do dispositivo conforme trecho abaixo no `onCreate`. Nos próximos passos, após receber a localização, uma chamada da função requisição de clima inclusa em `getLastLocation` será utilizada utilizando internamente o Volley de forma assíncrona em outra thread; quando a thread for concluída elá preencherá os dados da UI posteriormente ao implementartos os métodos `getLastLocation` e `requestWheather`. Tive que alterar os passos deste `How To` diminuindo os passos seccionados e inserindo funções completas para facilitar a conclusão do mesmo, infelizmente perderemos a sequencia minuciosa de passos mas facilitaremos a conclusão.

```java
override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_main)

    mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this)

//        val url = "https://api.hgbrasil.com/weather?key=4e56cf83&lat=-22.342742&log=-47.374483"
//
//        var queue = Volley.newRequestQueue(this)
//
//        var stringRequest = StringRequest(Request.Method.GET, url, Response.Listener<String> {
//            textView.text = "Resposta: ${it.substring(0, 500)}"
//            Log.d("RESPONSE"," - "+it)
//        }, Response.ErrorListener {
//            textView.text = it.localizedMessage
//            Log.d("RESPONSE ERROR"," - "+it.localizedMessage)
//        })
//        queue.add(stringRequest)

    queue = Volley.newRequestQueue(this)
        
    dialog = ProgressDialog(this)
    dialog.setTitle("Trabalhando")
    dialog.setMessage("Recuperando informações do Clima, aguarde...")
    dialog.show()

    if (checkPermissions(
            android.Manifest.permission.ACCESS_COARSE_LOCATION,
            android.Manifest.permission.ACCESS_FINE_LOCATION)) {
        //getLastLocation()
    }
}
```  
Neste ponto já podemos testar o fluxo de permissões de localização, voce deve comentar a função `getLastLocation` para testar por ela ainda não existe, ou prossiga e teste no final.
Caso teste agora, você pode testar novamente o app localizando as configurações de permissão em seu dispositivo e negando a permissão do app.
Ao negar, e executar novamente o app, você veráa explicação mais significativa oferecidao ao usuário para que ele conceda a permissão.
Agora já temos o fluxo de permissão necessário para obter a posição do dispositivo; vamos reescrever nossa requisição web com base na localização do dispositivo.

### Estruturar os dados recebidos pela requisição de previsão do tempo.
#### Criar Model Classes
Vamos estruturas os dados recebidos em JSON em objetos Java para facilitar sua manipulação.
1. Criar a classe Previsao. Crie o pacote `model` no pacote principal do app e dentro dele crie a classe `Previsao.kt`.
2. Adicione o código abaixo. Para aqueles que estiverem com versões mais antigas do Android Studio removam `: Serializable`:
```java
class Previsao : Serializable {

    var data: String? = null
    var diaDaSemana: String? = null
    var maxima: String? = null
    var minima: String? = null
    var descricao: String? = null
    var condicao: String? = null

    constructor(
        data: String, diaDaSemana: String, maxima: String, minima: String, descricao: String, condicao: String) {
        this.data = data
        this.diaDaSemana = diaDaSemana
        this.maxima = maxima
        this.minima = minima
        this.descricao = descricao
        this.condicao = condicao
    }

    constructor() {}

    companion object {
        private const val serialVersionUID = 1L
    }
}
```

3. Crie a classe `Clima.kt` no pacote `model`.
```java
package com.example.climaapp.model
class Clima {

    var temperatura: Int? = null
    var data: String? = null
    var hora: String? = null
    var codigoCondicao: String? = null
    var descricao: String? = null
    var atualmente: String? = null
    var cid: String? = null
    var cidade: String? = null
    var idImagem: String? = null
    var humidade: Int? = null
    var velocidadeDoVento: String? = null
    var nascerDoSol: String? = null
    var porDoSol: String? = null
    var condicaoDoTempo: String? = null
    var nomeDaCidade: String? = null

    var previsoes: List<Previsao>? = null

    constructor(
        temperatura: Int?,
        data: String,
        hota: String,
        codigoCondicao: String,
        descricao: String,
        atualmente: String,
        cid: String,
        cidade: String,
        idImagem: String,
        humidade: Int?,
        velocidadeDoVento: String,
        nascerDoSol: String,
        porDoSol: String,
        condicaoDoTempo: String,
        nomeDaCidade: String
    ) {
        this.temperatura = temperatura
        this.data = data
        this.hora = hota
        this.codigoCondicao = codigoCondicao
        this.descricao = descricao
        this.atualmente = atualmente
        this.cid = cid
        this.cidade = cidade
        this.idImagem = idImagem
        this.humidade = humidade
        this.velocidadeDoVento = velocidadeDoVento
        this.nascerDoSol = nascerDoSol
        this.porDoSol = porDoSol
        this.condicaoDoTempo = condicaoDoTempo
        this.nomeDaCidade = nomeDaCidade
    }

    constructor() {}
}
```
### Interface Gráfica
Com os modelos de dados necessários já criados, precisamos criar a interface gráfica do app.
1. Crie o Layout da item de lista em `res/layout/previsao_cell.xml`.
```xml
<?xml version="1.0" encoding="utf-8"?>
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
              xmlns:app="http://schemas.android.com/apk/res-auto" android:orientation="horizontal"
              android:layout_width="match_parent"
              android:layout_height="wrap_content" android:layout_margin="5dp">

    <ImageView
            android:layout_width="75dp"
            android:layout_height="wrap_content" app:srcCompat="@drawable/cloudy" android:id="@+id/imageViewIconWeather"
            android:layout_weight="1" android:cropToPadding="true" android:adjustViewBounds="true"
            android:tint="@android:color/white" android:padding="5dp" android:layout_margin="3dp"/>
    <LinearLayout
            android:orientation="vertical"
            android:layout_width="match_parent"
            android:layout_height="wrap_content" android:layout_weight="2" android:layout_margin="3dp">
        <TextView
                android:text="TextView"
                android:layout_width="match_parent"
                android:layout_height="match_parent" android:id="@+id/textViewTempoCelula" android:textSize="18sp"
                android:textColor="@android:color/white" android:layout_marginLeft="3dp"/>
        <TextView
                android:text="TextView"
                android:layout_width="match_parent"
                android:layout_height="wrap_content" android:id="@+id/textViewMaxMinCelula" android:textSize="14sp"
                android:textColor="@android:color/white" android:layout_marginLeft="3dp"/>
    </LinearLayout>
</LinearLayout>
```
2. Adicione o Layout Principal ao `activity_main.xml`.
```xml
<?xml version="1.0" encoding="utf-8"?>
<androidx.constraintlayout.widget.ConstraintLayout
        xmlns:android="http://schemas.android.com/apk/res/android"
        xmlns:tools="http://schemas.android.com/tools"
        xmlns:app="http://schemas.android.com/apk/res-auto"
        android:background="@drawable/gradient_animation"
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        android:id="@+id/root_layout"
        tools:context=".MainActivity">
    <TextView
            android:text="Araras"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:id="@+id/textViewCidade" android:textColor="@android:color/white"
            android:textSize="32sp"
            app:layout_constraintTop_toTopOf="parent"
            android:layout_marginTop="8dp" app:layout_constraintStart_toStartOf="parent"
            android:layout_marginStart="8dp"/>
    <TextView
            android:text='60"'
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:id="@+id/textViewTemperatura" android:textSize="120sp"
            android:textColor="@android:color/white"
            app:layout_constraintStart_toStartOf="parent"
            android:layout_marginStart="8dp"
            app:layout_constraintTop_toBottomOf="@+id/textViewData"/>
    <TextView
            android:text="12:57"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:id="@+id/textViewHora"
            android:textSize="24sp"
            android:textColor="@android:color/white"
            app:layout_constraintEnd_toEndOf="parent"
            android:layout_marginEnd="8dp" android:layout_marginTop="8dp"
            app:layout_constraintTop_toTopOf="@+id/textViewCidade"
            app:layout_constraintBottom_toBottomOf="@+id/textViewCidade"/>
    <ImageView
            android:layout_width="100dp"
            android:layout_height="100dp"
            android:id="@+id/imageViewIcon"
            app:layout_constraintTop_toTopOf="@+id/linearLayout"
            app:layout_constraintBottom_toBottomOf="@+id/linearLayout"
            app:layout_constraintStart_toEndOf="@+id/linearLayout" android:layout_marginStart="24dp"
            app:srcCompat="@drawable/cloud_day" android:backgroundTintMode="src_in"
            android:tint="@android:color/white"
            android:backgroundTint="@android:color/white"/>
    <LinearLayout
            android:orientation="vertical"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:id="@+id/linearLayout"
            app:layout_constraintTop_toTopOf="@+id/textViewTemperatura"
            app:layout_constraintBottom_toBottomOf="@+id/textViewTemperatura" android:layout_marginStart="8dp"
            app:layout_constraintStart_toEndOf="@+id/textViewTemperatura">
        <TextView
                android:text="Máx"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:id="@+id/textView3"
                android:textColor="@android:color/white"
                android:layout_weight="1" android:textAlignment="viewStart"
        />
        <TextView
                android:text="20"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:id="@+id/textViewMaxima"
                android:textColor="@android:color/white"
                android:layout_weight="1" android:textAlignment="viewStart" android:textStyle="bold"
                android:textSize="20sp"/>
        <TextView
                android:text="Mín"
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:id="@+id/textView8"
                android:textColor="@android:color/white"
                android:layout_weight="1" android:textAlignment="viewStart"/>
        <TextView
                android:text="20"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:id="@+id/textViewMinima"
                android:textColor="@android:color/white"
                android:layout_weight="1" android:textAlignment="viewStart" android:textSize="20sp"
                android:textStyle="bold"/>
    </LinearLayout>
    <TextView
            android:text="Nublado"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:id="@+id/textViewTempoCelula" android:textColor="@android:color/white"
            android:textSize="32sp"
            app:layout_constraintTop_toBottomOf="@+id/textViewTemperatura" app:layout_constraintStart_toStartOf="parent"
            android:layout_marginStart="8dp"/>
    <TextView
            android:text="05:07"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:id="@+id/textViewNascerDoSol"
            android:textColor="@android:color/white"
            android:layout_weight="1" android:textAlignment="viewStart"
            app:layout_constraintStart_toEndOf="@+id/textView11" android:layout_marginStart="5dp"
            app:layout_constraintTop_toTopOf="@+id/textView11"
            app:layout_constraintBottom_toBottomOf="@+id/textView11" android:textStyle="bold"/>
    <TextView
            android:text="Nascer do Sol: "
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:id="@+id/textView11"
            android:textColor="@android:color/white"
            android:layout_weight="1" android:textAlignment="viewStart"
            app:layout_constraintStart_toStartOf="@+id/textViewTempoCelula"
            app:layout_constraintTop_toBottomOf="@+id/textViewTempoCelula"/>
    <TextView
            android:text="17:50"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:id="@+id/textViewPorDoSol"
            android:textColor="@android:color/white"
            android:layout_weight="1" android:textAlignment="viewStart"
            app:layout_constraintStart_toEndOf="@+id/textView13" android:layout_marginStart="8dp"
            app:layout_constraintTop_toTopOf="@+id/textView13"
            app:layout_constraintBottom_toBottomOf="@+id/textView13" android:textStyle="bold"/>
    <TextView
            android:text="Por do Sol: "
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:id="@+id/textView13"
            android:textColor="@android:color/white"
            android:layout_weight="1" android:textAlignment="viewStart"
            app:layout_constraintStart_toEndOf="@+id/textViewNascerDoSol" android:layout_marginStart="32dp"
            app:layout_constraintTop_toTopOf="@+id/textViewNascerDoSol"
            app:layout_constraintBottom_toBottomOf="@+id/textViewNascerDoSol"/>
    <TextView
            android:text="TextView"
            android:layout_width="226dp"
            android:layout_height="wrap_content"
            android:id="@+id/textViewData"
            android:textColor="@android:color/white"
            android:textAlignment="viewStart"
            app:layout_constraintTop_toBottomOf="@+id/textViewCidade"
            app:layout_constraintStart_toStartOf="@+id/textViewCidade"/>
    <ListView
            android:layout_width="0dp"
            android:layout_height="0dp"
            android:id="@+id/listViewPrivisoes"
            android:layout_marginTop="8dp"
            app:layout_constraintTop_toBottomOf="@+id/textViewNascerDoSol"
            app:layout_constraintBottom_toBottomOf="parent" android:layout_marginBottom="8dp"
            app:layout_constraintStart_toStartOf="parent" android:layout_marginStart="8dp"
            app:layout_constraintEnd_toEndOf="parent" android:layout_marginEnd="8dp"/>
</androidx.constraintlayout.widget.ConstraintLayout>
```
### Binding de dados
Ao executar a requisição web, obteremos um conjunto de dados em JSON com os dados da previsão e utilizaremos nossos models para extrair os dados em JSON e armazená-los em objetos.

1. Remova o trecho de código comentado que realizava a requisição.
2. Crie a função que irá realizar a requisição conforme o trecho abaixo.
```javascript
private fun requestWheater(url: String): StringRequest {

        dialog.show()
        var stringRequest = StringRequest(Request.Method.GET, url, Listener<String> { result ->
            val jsonResult = JSONObject(result).getJSONObject("results")
            val jsonPresisoesList = jsonResult.getJSONArray("forecast")

            val clima = preencheClima(jsonResult, listaPrevisoes)
            preenchePrevisoes(jsonPresisoesList)

            // Preencher os dados de Clina no UI
            textViewCidade.text = clima.nomeDaCidade
            textViewTemperatura.text = "${clima.temperatura.toString()}˚"
            textViewHora.text = clima.hora
            textViewData.text = clima.data
            textViewMaxima.text = (clima.previsoes as ArrayList<Previsao>)[0].maxima
            textViewMinima.text = (clima.previsoes as ArrayList<Previsao>)[0].minima
            textViewTempoCelula.text = clima.descricao
            textViewNascerDoSol.text = clima.nascerDoSol
            textViewPorDoSol.text = clima.porDoSol
            textViewData.text =
                (clima.previsoes as ArrayList<Previsao>)[0].diaDaSemana?.toUpperCase()
                    .plus(" ").plus(clima.data)

            imageViewIcon.setImageResource(R.drawable.icon_snow)

            when (clima.condicaoDoTempo) {
                "storm" -> imageViewIcon.setImageResource(R.drawable.storm)
                "snow" -> imageViewIcon.setImageResource(R.drawable.snow)
                "rain" -> imageViewIcon.setImageResource(R.drawable.rain)
                "fog" -> imageViewIcon.setImageResource(R.drawable.fog)
                "clear_day" -> imageViewIcon.setImageResource(R.drawable.sun)
                "clear_night" -> imageViewIcon.setImageResource(R.drawable.moon)
                "cloud" -> imageViewIcon.setImageResource(R.drawable.cloudy)
                "cloudly_day" -> imageViewIcon.setImageResource(R.drawable.cloud_day)
                "cloudly_night" -> imageViewIcon.setImageResource(R.drawable.cloudy_night)
            }


            // Preencher ListView com a lista de Previsoes
            val adapter = PrevisaoAdapter(applicationContext, listaPrevisoes)
            listViewPrivisoes.adapter = adapter
            adapter.notifyDataSetChanged()

            dialog.dismiss()


            Log.d("RESPONSE: ", result.toString())
        }, Response.ErrorListener {
            Log.e("ERROR: ", it.localizedMessage)
        })

        queue.add(stringRequest)

        return stringRequest
    }
```
Com este trecho já possuímos os objetos com os dados da requisição serializados em JSON. Agora vamos implementar as funções `helpers` que utilizamos em `requestWheater` para o binding de dados.
3. Crie uma função que preencha um objeto do tipo Clima com os dados do objeto `jsonResult`; insira o código abaixo na função `requestWheater`.
```java
private fun preencheClima(jsonObject: JSONObject, listaPrevisoes: ArrayList<Previsao>): Clima {
        val clima = Clima(
            jsonObject.getInt("temp"),
            jsonObject.getString("date"),
            jsonObject.getString("time"),
            jsonObject.getString("condition_code"),
            jsonObject.getString("description"),
            jsonObject.getString("currently"),
            jsonObject.getString("cid"),
            jsonObject.getString("city"),
            jsonObject.getString("img_id"),
            jsonObject.getInt("humidity"),
            jsonObject.getString("wind_speedy"),
            jsonObject.getString("sunrise"),
            jsonObject.getString("sunset"),
            jsonObject.getString("condition_slug"),
            jsonObject.getString("city_name")
        )
        clima.previsoes = listaPrevisoes
        return clima
    }
```
4. Crie uma função que preencha uma lista de objetos do tipo Previsao para utilizarmos em nossa lista na interface gráfica; insira a função abaixo.
```java
private fun preenchePrevisoes(previsoes: JSONArray) {
        for (i in 0 until previsoes.length()) {
            val previsaoObject = previsoes.getJSONObject(i)
            val previsao = Previsao(
                previsaoObject.getString("date"),
                previsaoObject.getString("weekday"),
                previsaoObject.getString("max"),
                previsaoObject.getString("min"),
                previsaoObject.getString("description"),
                previsaoObject.getString("condition")
            )
            listaPrevisoes.add(previsao)
        }
    }
```
5. Crie o pacote `adapter` no pacote principal e crie a classe `Adapter` para preparar os dados para preencehr a lista de previsoes na UI.
```javascript

class PrevisaoAdapter(private val context: Context, private val dataSource: ArrayList<Previsao>): BaseAdapter() {

    @SuppressLint("ViewHolder")
    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val rowView = LayoutInflater.from(context).inflate(R.layout.previsao_cell, null, true)
        val textViewTempo = rowView.findViewById<TextView>(R.id.textViewTempoCelula)
        val textViewMaxMin= rowView.findViewById<TextView>(R.id.textViewMaxMinCelula)
        val imageViewClima = rowView.findViewById<ImageView>(R.id.imageViewIconWeather)

        val previsao = getItem(position) as Previsao

        textViewTempo.text = previsao.diaDaSemana?.toUpperCase()
            .plus(" - ").plus(previsao.descricao)
        textViewMaxMin.text = previsao.data.plus(" Máx: ")
            .plus(previsao.maxima).plus("° Mín: ").plus(previsao.minima).plus("°")

        when(previsao.condicao){
            "storm" -> imageViewClima.setImageResource(R.drawable.storm)
            "snow" -> imageViewClima.setImageResource(R.drawable.snow)
            "rain" -> imageViewClima.setImageResource(R.drawable.rain)
            "fog" -> imageViewClima.setImageResource(R.drawable.fog)
            "clear_day" -> imageViewClima.setImageResource(R.drawable.sun)
            "clear_night" -> imageViewClima.setImageResource(R.drawable.moon)
            "cloud" -> imageViewClima.setImageResource(R.drawable.cloudy)
            "cloudly_day" -> imageViewClima.setImageResource(R.drawable.cloud_day)
            "cloudly_night" -> imageViewClima.setImageResource(R.drawable.cloudy_night)
        }

        return rowView
    }

    override fun getItem(position: Int): Any {
        return dataSource[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return dataSource.size
    }

}
```
6. Atualize o `onCreate` para adicionar o efeito Degrade ao UI.
```java
override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //<editor-fold desc="Gradiente Effect" defaultstate="collapsed">
        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_FULLSCREEN
        val animDrawable = root_layout.background as AnimationDrawable
        animDrawable.setEnterFadeDuration(10)
        animDrawable.setExitFadeDuration(5000)
        animDrawable.start()
        //</editor-fold>

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this)

        queue = Volley.newRequestQueue(this)

        url =
            "https://api.hgbrasil.com/weather?key=4e56cf83&lat=${latitude}&log=${longitude}&user_ip=remote"

        dialog = ProgressDialog(this)
        dialog.setTitle("Trabalhando")
        dialog.setMessage("Recureando informações do Clima, aguarde...")
        dialog.show()

        getLastLocation()
    }
```
7. Adicione as imagens no diretório no início desta página ao diretório `res/drawables`, assim como e os arquivos de degrade em xml.
8. Atualize as cores a serem utilizadas no gradiente no arquivo `res/values/colors`.
```javascript 
<?xml version="1.0" encoding="utf-8"?>
<resources>
    <color name="colorPrimary">#CA00F9</color>
    <color name="colorPrimaryDark">#4E0DFD</color>
    <color name="colorAccent">#D81B60</color>
    <color name="colorTransparent">#00ffffff</color>

    <color name="colorGradientStart">#4E0DFD</color>
    <color name="colorGradientCenter">#CA00F9</color>
    <color name="colorGradientEnd">#FD0D5D</color>
</resources>
```
9. Terminamos!
10. Teste o ClimApp!

